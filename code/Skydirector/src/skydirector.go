package main

import (
	"fmt"
	"net/http"
	"github.com/fsouza/go-dockerclient"
)

type Webserver struct {}

func main() {
	s := Webserver{}
	http.HandleFunc("/skydirector/", s.handler)
	http.ListenAndServe(":8080", nil)
}

func (s *Webserver) handler(w http.ResponseWriter, r *http.Request) {
    
    //connect to docker
	endpoint := "unix:///var/run/docker.sock"
    client, _ := docker.NewClient(endpoint)

    //handle arguments
    r.ParseForm()
    values := r.Form
    if len(values["stopId"]) > 0 {
    	client.StopContainer(values["stopId"][0], 15)
    	client.RemoveContainer(docker.RemoveContainerOptions{values["stopId"][0], true, true})
    }
    if len(values["ip"]) > 0 && len(values["port"]) > 0 {
    	ip := values["ip"][0]
    	port :=  values["port"][0]
    	cmd := []string{ip, port}
    	containerConfig := new(docker.Config)
    	containerConfig.Cmd = cmd
    	containerConfig.Image = "dr3la/skystorage"
    	container,_ := client.CreateContainer(docker.CreateContainerOptions{"Node_" +  ip + "_" + port, containerConfig})
    	config := new(docker.HostConfig)
    	config.NetworkMode = "host"
    	client.StartContainer(container.ID, config)
    }

    //output page
    fmt.Fprintf(w, "<html><head><title>Sky Director</title></head>")
	fmt.Fprintf(w, "<body><h1>Sky Director</h1>")

	//add new container
	fmt.Fprintf(w, "<h4>Add node</h4>")
	fmt.Fprintf(w, "<form method=\"post\">")
	fmt.Fprintf(w, "<table><tr><td>IP</td><td><input type=\"text\" name=\"ip\" /></td></tr>")
	fmt.Fprintf(w, "<tr><td>Port</td><td><input type=\"text\" name=\"port\" /></td></tr>")
	fmt.Fprintf(w, "<tr><td>Remote IP</td><td><input type=\"text\" name=\"remoteip\" /></td></tr>")
	fmt.Fprintf(w, "<tr><td>Remote Port</td><td><input type=\"text\" name=\"remoteport\" /></td></tr>")
	fmt.Fprintf(w, "<tr><td /><td><input type=\"submit\" value=\"Add node\" /></td></tr></table></form>")

	//list containers
    options := docker.ListContainersOptions{true, true, 1000, "", ""}
    containers, _ := client.ListContainers(options)
    fmt.Fprintf(w, "<h4>Running nodes</h4><table>")

    for _, containers := range containers {
    	fmt.Fprintf(w, "<tr><td>" + containers.ID + "</td>")
    	fmt.Fprintf(w, "<td><form method=\"post\"><input type=\"hidden\" name=\"stopId\" value=\"" + containers.ID + "\"><input type=\"submit\" value=\"Stop\"></form></td></tr>")
    }

	fmt.Fprintf(w, "</body>")
}