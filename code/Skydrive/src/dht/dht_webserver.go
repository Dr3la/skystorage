package dht

import (
	"fmt"
	"net/http"
)

type Webserver struct {
	node *Node
}
var served bool = false

func (s *Webserver) start() {
	if(!served) {
		served = true
		http.HandleFunc("/", s.handler)
		http.ListenAndServe(":80", nil)
	}
}

func (s *Webserver) handler(w http.ResponseWriter, r *http.Request) {

	method := r.Method // Get HTTP Method (string)
    r.ParseForm()      // Populates request.Form
    values := r.Form

    fmt.Fprintf(w, "<html><head><title>Sky Repo</title></head>")
	fmt.Fprintf(w, "<body><h1>Sky Repo</h1>")
	
	switch method {
	case "GET": s.handleGET(w, values)
	case "POST": 
		if len(values["remove"]) > 0{
 			s.handleREMOVE(w, values)
 		}else{
			s.handlePOST(w, values)
		}
	case "PUT": s.handlePOST(w, values)
	case "DELETE": s.handleREMOVE(w, values)
	} 

	//html form: GET
	fmt.Fprintf(w, "<b>Get</b><br />")
	fmt.Fprintf(w, "<form name=\"input\" action=\"/\" method=\"get\">")
	fmt.Fprintf(w, "<input type=\"text\" name=\"get\" />")
	fmt.Fprintf(w, "<input type=\"submit\" value=\"Get\"></form>")

	//html form: POST
	fmt.Fprintf(w, "<b>Set / Update</b><br />")
	fmt.Fprintf(w, "<form name=\"input\" action=\"/\" method=\"post\">")
	fmt.Fprintf(w, "<table><tr><td><span>Key</span></td>")
	fmt.Fprintf(w, "<td><input type=\"text\" name=\"key\" /></td></tr>")
	fmt.Fprintf(w, "<tr><td><span>Hex value</span></td>")
	fmt.Fprintf(w, "<td><input type=\"text\" name=\"value\" />")
	fmt.Fprintf(w, "<input type=\"submit\" value=\"Set / Update\"></td></tr></table></form>")
	
	//html form: REMOVE
	fmt.Fprintf(w, "<b>Delete</b><br />")
	fmt.Fprintf(w, "<form name=\"input\" action=\"/\" method=\"post\">")
	fmt.Fprintf(w, "<input type=\"text\" name=\"remove\" />")
	fmt.Fprintf(w, "<input type=\"submit\" value=\"Remove\"></form>")


	fmt.Fprintf(w, "</body>")
}

func (s *Webserver)  handleGET(w http.ResponseWriter, keys map[string][]string) {
	if len(keys["get"]) > 0 {
		k := keys["get"][0]
		data := *s.node.get(k)
		fmt.Fprintf(w, "<p>" + k + ": " + fmt.Sprintf("%x", data) + "</p>")
	}
	
}

func (s *Webserver)  handlePOST(w http.ResponseWriter, values map[string][]string) {
	if len(values["key"]) > 0 && len(values["value"]) > 0 && values["value"][0] != "" {
		k := values["key"][0]
		v := values["value"][0]
		s.node.set(k, toByteArray(v))
		fmt.Fprintf(w, "<p>Stored " + k + ": " + v + "</p>")
	} else {
		fmt.Fprintf(w, "<p>Failed to store!</p>")
	}
}

func (s *Webserver)  handleREMOVE(w http.ResponseWriter, values map[string][]string) {
	if len(values["remove"]) > 0 {
		k := values["remove"][0]
		s.node.remove(k)
		fmt.Fprintf(w, "<p> key " + k + ": deleted</p>")
	}
}
