package dht

import (
	"fmt"
	"net"
	"net/rpc"
	"os"
)

type Finger struct {
	start string
	successor *RemoteNode
}

type RemoteNode struct {
	NodeId, Host, Port string
}

type UpdateOthersData struct {
	Remote RemoteNode
	I int
}

type DataTuple struct {
	HashedKey string
	Data []byte
	Replica bool
}

type RemoveData struct {
	HashedKeys []string 
	Replica bool
}

type Node struct {
	nodeId, host, port string
	finger []Finger
	precedessor *RemoteNode
	data map[string][]byte
	replicatedData map[string][]byte
}

func StartNode() {
	if len(os.Args) < 3 || len(os.Args)  > 5 {
		fmt.Println("Usage: go dht_node.go host port [remoteNodeHost remoteNodePort]")
		return
	}
	n := makeDHTNode(nil, os.Args[1], os.Args[2])
	if len(os.Args) == 3 || len(os.Args) == 4 {
		n.addToRing(n.toRemote())
	} else {
		ip := os.Args[3]
		port := os.Args[4]
		r := RemoteNode{n.getNodeIdStub(ip, port), ip, port}
		n.addToRing(&r)
	}
}

func makeDHTNode(nodeId *string, host, port string) Node {
	var id string
	if nodeId == nil {
		id = generateNodeId()
	}else{
		id = sha1hash(*nodeId)
	}
	return Node{id, host, port, make([]Finger, 160), nil, make(map[string][]byte), make(map[string][]byte)}
}

func checkError(err error) {
	if err != nil {
		fmt.Println("error ", err)
	}
} 

func (n Node) testCalcFingers(a, b int) {} //Only for compiling

func (n *Node) toRemote() *RemoteNode {
	return &RemoteNode{n.nodeId, n.host, n.port}
}

/*************************************************************
 * PRINT_RING
 *************************************************************/
 //TODO: if there is only one node in the ring, that node should be printed
func (n *Node) printRing() {
	n.printRingStub([]string{})
}

func (n *Node) printRingStub(nodes []string) {
	if n.nodeId != n.finger[0].successor.NodeId {
		nodes = append(nodes, n.nodeId)
		remote := n.finger[0].successor
		client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
		checkError(err)
		var reply *int
		err = client.Call("Node.PrintRingRpc", nodes, &reply)
		checkError(err)
	}
}

func (n *Node) PrintRingRpc(nodes []string, reply *int) error {
	if nodes[0] == n.nodeId {
		for _, e := range nodes {
			fmt.Println(e)
		}
	} else {
		go n.printRingStub(nodes)
	}
	*reply = 0
	return nil
}

/*************************************************************
 * ADD_TO_RING
 * n: Node to be added. remote: existing node in the ring
 *************************************************************/
func (n *Node) addToRing(remote *RemoteNode) {
	if n.nodeId != remote.NodeId {
		n.initFingerTable(remote)
		n.updateOthers()
	} else {
		for i := 1; i <= 160; i++ {
			start := calcFinger(toByteArray(n.nodeId), i, 160, true)
			n.finger[i-1] = Finger{start, n.toRemote()}
		}
		n.precedessor = n.toRemote()
	}
	//n.printRing()

	//return our node id
	fmt.Println("***** nodeID of the new node *****")
	fmt.Println(n.nodeId)
	fmt.Println("**********************************")

	//Start RPC server
	go n.startServer()

	//Start HTTP server
	server := Webserver{n}
	go server.start()
}

func (n *Node) startServer() {
	s := rpc.NewServer()
	s.Register(n)
	tcpAddr, err := net.ResolveTCPAddr("tcp", ":" + n.port)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	s.Accept(listener)
}


/*************************************************************
 * FIND_SUCCESSOR
 *************************************************************/
func (n *Node) findSuccessor(id string) *RemoteNode {
	//if n.nodeId == id: finds successor of n. If this is not desired, 1 could be subtracted from the id
	precedessor := n.findPrecedessor(id)
	return n.getSuccessorStub(precedessor)
}

func (n *Node) findPrecedessor(id string) *RemoteNode {
	if !between(toByteArray(n.nodeId), toByteArray(n.finger[0].successor.NodeId), toByteArray(id)) {
		return n.findPrecedessorStub(id, n.closestPrecedingFinger(id))
	} else {
		return n.toRemote()
	}
}

func (n *Node) findPrecedessorStub(id string, remote *RemoteNode) *RemoteNode {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *RemoteNode
	err = client.Call("Node.FindPrecedessorRpc", id, &reply)
	checkError(err)
	return reply	
}

func (n *Node) FindPrecedessorRpc(id string, reply *RemoteNode) error {
	*reply = *n.findPrecedessor(id)
	return nil
}

func (n *Node) closestPrecedingFinger(id string) *RemoteNode {
	for i:=159; i>=0; i--{
		if between(toByteArray(n.nodeId),toByteArray(id),toByteArray(n.finger[i].successor.NodeId)){
			return n.finger[i].successor
		}
	}
	return n.toRemote()
}

/*************************************************************
 * LOOKUP
 *************************************************************/
func (n *Node) lookupStub(hashkey string, remote *RemoteNode) *RemoteNode {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *RemoteNode
	err = client.Call("Node.LookupRpc", hashkey, &reply)
	checkError(err)
	return reply
}

func (n *Node) LookupRpc(hashkey string, reply *RemoteNode) error {
	remote := n.lookup(hashkey)
	*reply = *remote
	return nil
}

func (n *Node) lookup(hashkey string) *RemoteNode {
	if hashkey == n.nodeId {
		return n.toRemote()
	} else if between(toByteArray(n.nodeId),toByteArray(n.finger[0].successor.NodeId),toByteArray(hashkey)) {
		return n.finger[0].successor
	} else {
		return n.lookupStub(hashkey, n.finger[0].successor)
	}
}

/*************************************************************
 * INIT_FINGER_TABLE
 * n: Node to be added. remote: existing node in the ring
 *************************************************************/
func (n *Node) initFingerTable(remote *RemoteNode) {
	start := calcFinger(toByteArray(n.nodeId), 1, 160, true)
	successor := n.lookupStub(start, remote)
	n.finger[0] = Finger{start, successor}
	n.precedessor = n.getPrecedessorStub(successor)
	n.data = *n.setPrecedessorStub(successor, n.toRemote())
	n.replicatedData = *n.setSuccessorStub(n.precedessor, n.toRemote())

	for i := 1; i < 160; i++ {
		start = calcFinger(toByteArray(n.nodeId), i + 1, 160, true)
		if between(toByteArray(n.nodeId),toByteArray(n.finger[i-1].successor.NodeId),toByteArray(start)){
			n.finger[i] = Finger{start, n.finger[i-1].successor}
		}else{
			successor = n.lookup(start)
			n.finger[i] = Finger{start, successor}
		}
	}
}

/*************************************************************
 * GET_PRECEDESSOR
 *************************************************************/
func (n *Node) getPrecedessorStub(remote *RemoteNode) *RemoteNode {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *RemoteNode
	err = client.Call("Node.GetPrecedessorRpc", 0, &reply)
	checkError(err)
	return reply
}

func (n *Node) GetPrecedessorRpc(dummy int, reply *RemoteNode) error {
	remote := n.precedessor
	*reply = *remote
	return nil
}

/*************************************************************
 * SET_PRECEDESSOR
 *************************************************************/
func (n *Node) setPrecedessorStub(remote *RemoteNode, setTo *RemoteNode) *map[string][]byte {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *map[string][]byte
	err = client.Call("Node.SetPrecedessorRpc", setTo, &reply)
	checkError(err)
	return reply
}

 func (n *Node) SetPrecedessorRpc(remote RemoteNode, reply *map[string][]byte) error {
 	n.precedessor = &remote

 	// Because the data of our old precesessor is now replicated in our new
 	// precedessor, we no longer need to store a replica.
 	n.replicatedData =  make(map[string][]byte)
	
	//decide for which of our own data the precesessor is now responsible
 	toPrecedessor := make(map[string][]byte)
 	for k, v := range n.data {
 		if !between(toByteArray(n.precedessor.NodeId), toByteArray(n.nodeId), toByteArray(k)) {
 			//our precedessors data
 			toPrecedessor[k] = v
 			n.replicatedData[k] = v
 			delete(n.data, k)
 		}
	}

	//remove keys me moved to the new precedessor from the replica list of our successor
	if len(toPrecedessor) > 0 {
		keys := make([]string, 0, len(toPrecedessor))
	    for k := range toPrecedessor {
	        keys = append(keys, k)
	    }
		go n.removeStub(keys, true, n.finger[0].successor)
	}

	*reply = toPrecedessor 
	return nil
}

/*************************************************************
 * GET_SUCCESSOR
 *************************************************************/
func (n *Node) getSuccessorStub(remote *RemoteNode) *RemoteNode {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *RemoteNode
	err = client.Call("Node.GetSuccessorRpc", 0, &reply)
	checkError(err)
	return reply
}

func (n *Node) GetSuccessorRpc(dummy int, reply *RemoteNode) error {
	remote := n.finger[0].successor
	*reply = *remote
	return nil
}

/*************************************************************
 * SET_SUCCESSOR
 *************************************************************/
func (n *Node) setSuccessorStub(remote *RemoteNode, setTo *RemoteNode) *map[string][]byte {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *map[string][]byte
	err = client.Call("Node.SetSuccessorRpc", setTo, &reply)
	checkError(err)
	return reply
}

 func (n *Node) SetSuccessorRpc(remote RemoteNode, reply *map[string][]byte) error {
 	n.finger[0].successor = &remote
	*reply = n.data
	return nil
}

/*************************************************************
 * UPDATE_OTHERS
 *************************************************************/
func (n *Node) updateOthers() {
	for i := 1; i <= 160; i++ {
		start := calcFinger(toByteArray(n.nodeId), i, 160, false)

		startNode := n.lookup(start)
		var p *RemoteNode
		if startNode.NodeId != n.nodeId {
			p = n.getPrecedessorStub(startNode)
		} else {
			p = n.precedessor
		}

		go n.updateFingerTableStub(p, n.toRemote(), i)
	}
}

/*************************************************************
 * UPDATE_FINGER_TABLE
 *************************************************************/
 //update existing node remote because n was added to the nework
func (n *Node) updateFingerTableStub(existing *RemoteNode, newNode *RemoteNode, i int) {
	data := UpdateOthersData{*newNode, i}
	remote := existing
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *int
	err = client.Call("Node.UpdateFingerTableRpc", data, &reply)
	checkError(err)
}

//update existing node n, because data.Remote / s was added to the network
func (n *Node) UpdateFingerTableRpc(data UpdateOthersData, reply *int) error {	
	s := data.Remote
	i := data.I
	if between(toByteArray(n.nodeId), toByteArray(n.finger[i-1].successor.NodeId), toByteArray(s.NodeId)) && n.nodeId != s.NodeId {
		n.finger[i-1].successor = &s
		p := n.precedessor
		go n.updateFingerTableStub(p, &s, i)
	}
	*reply = 0
	return nil
}

/*************************************************************
 * SET
 *************************************************************/
func (n *Node) set(key string, data []byte) {
	hash := sha1hash(key)
	responsibleNode := n.findSuccessor(hash)
	n.setStub(hash, data, false, responsibleNode)
}

func (n *Node) setStub(hashedKey string, data []byte, replica bool, remote *RemoteNode) {
	arg := DataTuple{hashedKey, data, replica}
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *int
	err = client.Call("Node.SetRpc", arg, &reply)
	checkError(err)	
}

func (n *Node) SetRpc(arg DataTuple, reply *int) error {
	if arg.Replica {
		n.replicatedData[arg.HashedKey] = arg.Data
	} else {
		n.data[arg.HashedKey] = arg.Data
		go n.setStub(arg.HashedKey, arg.Data, true, n.finger[0].successor)
	}	
	*reply = 0
	return nil
}

/*************************************************************
 * GET
 *************************************************************/
//returns data stored for the key. If no data is found, an empty byte array is returned
func (n *Node) get(key string) *[]byte {
	hash := sha1hash(key)
	responsibleNode := n.findSuccessor(hash)
	return n.getStub(hash, responsibleNode)
}

//returns data stored for the key. If no data is found, an empty byte array is returned
func (n *Node) getStub(hashedKey string, remote *RemoteNode) *[]byte {
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *[]byte
	err = client.Call("Node.GetRpc", hashedKey, &reply)
	checkError(err)
	return reply
}

func (n *Node) GetRpc(hashedKey string, reply *[]byte) error {

	val, contained := n.data[hashedKey]
	if !contained {
		*reply = n.replicatedData[hashedKey]
	}
	*reply = val
	return nil
}

/*************************************************************
 * REMOVE
 *************************************************************/
func (n *Node) remove(key string) {
	hash := sha1hash(key)
	responsibleNode := n.findSuccessor(hash)
	n.removeStub([]string{hash}, false, responsibleNode)
}

func (n *Node) removeStub(hashedKeys []string, replica bool, remote *RemoteNode) {
	arg := RemoveData{hashedKeys, replica}
	client, err := rpc.Dial("tcp", remote.Host + ":" + remote.Port)
	checkError(err)
	var reply *int
	err = client.Call("Node.RemoveRpc", arg, &reply)
	checkError(err)
}

func (n *Node) RemoveRpc(arg RemoveData, reply *int) error {
	if arg.Replica {
		for _, k := range arg.HashedKeys {
			delete(n.replicatedData, k)
		}		
	} else {
		for _, k := range arg.HashedKeys {
			delete(n.data, k)
		}	
		go n.removeStub(arg.HashedKeys, true, n.finger[0].successor)
	}	
	*reply = 0
	return nil
}

/*************************************************************
 * GET_NODE_ID
 *************************************************************/
 func (n *Node) getNodeIdStub(ip string, port string) string {
	client, err := rpc.Dial("tcp", ip + ":" + port)
	checkError(err)
	var reply *string
	err = client.Call("Node.GetNodeIdRpc", 0, &reply)
	checkError(err)
	return *reply
}

func (n *Node) GetNodeIdRpc(arg int, reply *string) error {
	*reply = n.nodeId
	return nil
}