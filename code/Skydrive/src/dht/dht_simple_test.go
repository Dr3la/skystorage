package dht

import (
	"fmt"
	"testing"
	"time"
	"math"
	//"github.com/stretchr/testify/assert"
)

// test cases can be run by calling e.g. go test -test.run TestRingPrint

func TestRingFinger(t *testing.T) {
	id1:="01"
	node1 := makeDHTNode(&id1, "localhost", "1337")
	node1.addToRing(node1.toRemote())
	id2:="02"
	node2 := makeDHTNode(&id2, "localhost", "1338")
	node2.addToRing(node1.toRemote())
	id3:="03"
	node3 := makeDHTNode(&id3, "localhost", "1339")
	node3.addToRing(node2.toRemote())
	
	
	//let the network settle
	time.Sleep(time.Second * 5)
	//printNodeWithFingers(node3)
	//fmt.Println(node1.findSuccessor("ddfe163345d338193ac2bdc183f8e9dcff904b42"))

	//store test
	node1.set("foo", toByteArray("ddfe"))
	node3.set("blah", toByteArray("ddfe"))
	node3.set("foo", toByteArray("01030307"))
	fmt.Println(node2.get("blah"))

	time.Sleep(time.Second * 1)
	node1.remove("foo")

	id4:="04"
	node4 := makeDHTNode(&id4, "localhost", "1340")
	node4.addToRing(node2.toRemote())

	printNode(node1)
	printNode(node2)
	printNode(node3)
	printNode(node4)


	time.Sleep(time.Second * math.MaxInt32)
}

func printNode(node Node) {
	fmt.Println("Node", node.nodeId)
	fmt.Println("    start", node.finger[0].start)
	fmt.Println("    successor", node.finger[0].successor.NodeId)
	fmt.Println("    precedessor", node.precedessor.NodeId)

	if len(node.data) > 0 { fmt.Println("    data:") }
	for k, v := range node.data {
		fmt.Println("        ", k, v)
	}

	if len(node.replicatedData) > 0 { fmt.Println("    replicas:") }
	for k, v := range node.replicatedData {
		fmt.Println("        ", k, v)
	}

	fmt.Println()
}

func printNodeWithFingers(node Node) {
	fmt.Println("Node", node.nodeId)
	for i:=0; i<160; i++{
		fmt.Println("    ", node.finger[i].start, node.finger[i].successor.NodeId)
	}
}