package dht

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"math/big"
	"encoding/hex"
)

func distance(a, b []byte, bits int) *big.Int {
	var ring big.Int
	ring.Exp(big.NewInt(2), big.NewInt(int64(bits)), nil)

	var a_int, b_int big.Int
	(&a_int).SetBytes(a)
	(&b_int).SetBytes(b)

	var dist big.Int
	(&dist).Sub(&b_int, &a_int)

	(&dist).Mod(&dist, &ring)
	return &dist
}

func between(id1, id2, key []byte) bool {//true if key e [a,b)
	// 0 if a==b, -1 if a < b, and +1 if a > b

	if bytes.Compare(key, id1) == 0 { // key == id1
		return true
	}

	//Added by Hector and Robin
	//if bytes.Compare(id1,id2) == 0 { //id1 == id2 && key != id1
	//	return false
	//}

	if bytes.Compare(id2, id1) == 1 { // id2 > id1
		if bytes.Compare(key, id2) == -1 && bytes.Compare(key, id1) == 1 { // key < id2 && key > id1
			return true
		} else {
			return false
		}
	} else { // id1 >= id2
		if bytes.Compare(key, id1) == 1 || bytes.Compare(key, id2) == -1 { // key > id1 || key < id2
			return true
		} else {
			return false
		}
	}
}
func between2(id1, id2, key []byte) bool {//true if key e [a,b]
	// 0 if a==b, -1 if a < b, and +1 if a > b

	//Added by Hector and Robin
	//if bytes.Compare(id1,id2) == 0 { //id1 == id2 && key != id1
	//	return false
	//}
	if bytes.Compare(key, id1) == 0 { // key == id1
		return true
	}
	if bytes.Compare(key, id2) == 0 { // key == id1
		return true
	}
	if bytes.Compare(id2, id1) == 1 { // id2 > id1
		if bytes.Compare(key, id2) == -1 && bytes.Compare(key, id1) == 1 { // key < id2 && key > id1
			return true
		} else {
			return false
		}
	} else { // id1 >= id2
		if bytes.Compare(key, id1) == 1 || bytes.Compare(key, id2) == -1 { // key > id1 || key < id2
			return true
		} else {
			return false
		}
	}
}

//case plus: (n + 2^(k-1)) mod (2^m)
//case minus: (n + 2^(k-1) + 1)  mod (2^m)
func calcFinger(n []byte, k int, m int, plus bool) (string) { //Original: (string, []bytes) removed by Hector and Robin
	//fmt.Println("calulcating result = (n+2^(k-1)) mod (2^m)")

	// convert the n to a bigint
	nBigInt := big.Int{}
	nBigInt.SetBytes(n)

	//fmt.Printf("n            %s\n", nBigInt.String())
	//fmt.Printf("k            %d\n", k)
	//fmt.Printf("m            %d\n", m)

	// get the right addend, i.e. 2^(k-1)
	two := big.NewInt(2)
	addend := big.Int{}
	addend.Exp(two, big.NewInt(int64(k-1)), nil)

	//fmt.Printf("2^(k-1)      %s\n", addend.String())

	// calculate sum
	sum := big.Int{}
	if plus {
		sum.Add(&nBigInt, &addend)
	} else {
		one := big.NewInt(1)
		sum.Sub(&nBigInt, &addend)
		sum.Add(&sum, one)
	}	

	//fmt.Printf("(n+2^(k-1))  %s\n", sum.String())

	// calculate 2^m
	ceil := big.Int{}
	ceil.Exp(two, big.NewInt(int64(m)), nil)

	//fmt.Printf("2^m          %s\n", ceil.String())

	// apply the mod
	result := big.Int{}
	result.Mod(&sum, &ceil)

	//fmt.Printf("finger       %s\n", result.String())

	resultBytes := result.Bytes()
	resultHex := fmt.Sprintf("%x", resultBytes)

	//fmt.Printf("finger (hex) %s\n", resultHex)

	return resultHex//, resultBytes Removed by Hector and Robin
}

func generateNodeId() string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	return sha1hash(u.String())
}

func sha1hash(str string) string {
	// calculate sha-1 hash
	hasher := sha1.New()
	hasher.Write([]byte(str))

	return fmt.Sprintf("%x", hasher.Sum(nil))
}

func toByteArray(str string) []byte {
	a,e := hex.DecodeString(str)
	if e != nil {
		fmt.Println(e)
	}
	return a
}
