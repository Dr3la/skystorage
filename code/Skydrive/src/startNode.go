package main

import (
	"./dht"
	"time"
	"math"
)

func main() {
	dht.StartNode()
	
	//sleep
	time.Sleep(time.Second * math.MaxInt32)
}