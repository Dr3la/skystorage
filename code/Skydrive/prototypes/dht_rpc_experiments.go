package main

import (
	"fmt"
	"net"
	"os"
	"net/rpc"
	"net/http"
	"strconv"
)

type VectorArgs struct {
	X, Y int
}

type StringArgs struct {
	Str string
}

func main() {
	if len(os.Args) == 1 {
		receive()
	} else {
		send()
	}
}

type Server int
func (t *Server) ReceiveVector(args *VectorArgs, reply *int) error {
	fmt.Println(args.X, args.Y)
	*reply = args.X + args.Y
	return nil
}

func (t *Server) ReceiveString(args *StringArgs, reply *int) error {
	fmt.Println(args.Str)
	*reply = 0
	return nil
}

func receive() {
	server := new(Server)
	rpc.Register(server)
	rpc.HandleHTTP()
	l, err := net.Listen("tcp", ":1337")
	if err != nil {
		fmt.Println("error", err)
	}
	http.Serve(l, nil)
}

func send() {
	client, err := rpc.DialHTTP("tcp", "192.168.0.109:1337")
	if err != nil {
		fmt.Println("error", err)
	}

	var reply int
	if len(os.Args) == 3 {
		a,_ :=strconv.Atoi(os.Args[1])
		b,_ :=strconv.Atoi(os.Args[2])
		args := VectorArgs{a, b}
		err = client.Call("Server.ReceiveVector", &args, &reply)
		if err != nil {
			fmt.Println("error", err)
		}
	} else {
		args := StringArgs{os.Args[1]}
		err = client.Call("Server.ReceiveString", &args, &reply)
		if err != nil {
			fmt.Println("error", err)
		}
	}

	fmt.Println(reply)
}