package dht

import (
	"fmt"
	//"time" we would use a timer to uptade the fingers table: func newTimer(d Duration)
)

type Finger struct {
	start string
	successor *Node
}

type Node struct { 
	nodeId, host, port string
	finger []Finger
	precedessor *Node 
}

func (n Node) testCalcFingers(a, b int) {} //TODO

func makeDHTNode(nodeId *string, host, port string) Node {
	var id string
	if nodeId == nil {
		id = generateNodeId()
	}else{
		id = sha1hash(*nodeId)
	}
	
	return Node{id, host, port, make([]Finger, 160), nil}
}

func (n *Node) printRing() {
	fmt.Println(n.nodeId)
	current := n.finger[0].successor
	for current != n {
		fmt.Println(current.nodeId)
		current = current.finger[0].successor
	}
}

func (n *Node) addToRing(other *Node) {
	if n != other {
		n.init_finger_table(other)
		other.update_others()
	} else {
		for i := 1; i <= 160; i++ {
			start := calcFinger(toByteArray(n.nodeId), i, 160, true)
			n.finger[i-1] = Finger{start, n}
		}
		n.precedessor = n
	}
}

/*func (n *Node) find_successor(id string) *Node {
	return n.find_precedessor(id).finger[0].successor
}

//WRONG
func (n *Node) find_precedessor(id string) *Node {
	nTmp := n
	for !between(toByteArray(nTmp.nodeId), toByteArray(nTmp.finger[0].successor.nodeId), toByteArray(id)){//id != nTmp.nodeId && id != nTmp.finger[0].successor.nodeId{
		//fmt.Println(nTmp.nodeId, id)
		nTmp = nTmp.closest_preceding_finger(id)
		if n == nTmp {
			return n //We completed a full circle
		}
	}
	return nTmp
}

func (n *Node) closest_preceding_finger(id string) *Node {
	for i:=159; i>=0; i--{
		if between(toByteArray(n.nodeId),toByteArray(id),toByteArray(n.finger[i].successor.nodeId)){
			return n.finger[i].successor
		}
	}
	return n
}*/

func (n *Node) lookup(hashkey string) *Node {
	if hashkey == n.nodeId {
		return n
	} else if between(toByteArray(n.nodeId),toByteArray(n.finger[0].successor.nodeId),toByteArray(hashkey)) {
		return n.finger[0].successor
	} else {
		return n.finger[0].successor.lookup(hashkey)
	}
}

func (n *Node) init_finger_table(other *Node) {
	start := calcFinger(toByteArray(other.nodeId), 1, 160, true)
	successor := n.lookup(start)
	other.finger[0] = Finger{start, successor}
	other.precedessor = successor.precedessor
	successor.precedessor = other
	other.precedessor.finger[0].successor = other

	for i := 1; i < 160; i++ {
		start = calcFinger(toByteArray(other.nodeId), i + 1, 160, true)
		if between(toByteArray(other.nodeId),toByteArray(other.finger[i-1].successor.nodeId),toByteArray(start)){
			other.finger[i] = Finger{start, other.finger[i-1].successor}
		}else{
			successor = n.lookup(start)
			other.finger[i] = Finger{start, successor}
		}
	}
}

func (n *Node) update_others() {
	for i := 1; i <= 160; i++ {
		start := calcFinger(toByteArray(n.nodeId), i, 160, false)
		p := n.lookup(start).precedessor
		p.update_finger_table(n, i)
		
	}
}

func (n *Node) update_finger_table(s* Node, i int) {
	if between(toByteArray(n.nodeId), toByteArray(n.finger[i-1].successor.nodeId), toByteArray(s.nodeId)) && n!=s{ //TODO: FIX THIS HACK
		n.finger[i-1].successor = s
		p := n.precedessor
		p.update_finger_table(s, i)
	}
}

